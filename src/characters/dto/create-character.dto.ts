export class CreateCharacterDto {
  name: string;
  description: string;
  image;
}
