import { Controller, Get, Post, Body, Res, HttpStatus } from '@nestjs/common';
import { CharactersService } from './characters.service';
import { CreateCharacterDto } from './dto/create-character.dto';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('api/character-db-api/v1/')
export class CharactersController {
  constructor(private readonly charactersService: CharactersService) {}
  @ApiBearerAuth()
  @Post()
  create(@Body() createCharacterDto: CreateCharacterDto, @Res() res) {
    this.charactersService
      .create(createCharacterDto)
      .then((result) => {
        return res.status(HttpStatus.OK).send(result);
      })
      .catch((e) => {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(e);
      });
  }

  @ApiBearerAuth()
  @Get()
  findAll(@Res() res) {
    this.charactersService
      .findAll()
      .then((result) => {
        return res.status(HttpStatus.OK).send(result);
      })
      .catch((e) => {
        return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(e);
      });
  }
}
