import { Injectable } from '@nestjs/common';
import { CreateCharacterDto } from './dto/create-character.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Character, CharacterDocument } from './entities/character.entity';
import { Model } from 'mongoose';

@Injectable()
export class CharactersService {
  constructor(
    @InjectModel(Character.name)
    private characterModel: Model<CharacterDocument>,
  ) {}
  async create(createCharacterDto: CreateCharacterDto): Promise<Character> {
    const createdCat = new this.characterModel(createCharacterDto);
    return await createdCat.save();
  }

  async findAll(): Promise<Character[]> {
    return await this.characterModel.find().exec();
  }
}
