import {
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common';
import { FirebaseAdminService } from '../service/firebase-admin.service';

export function getAuthorizationBearer(request: any): string {
  return request.headers.authorization.split(' ')[1];
}
@Injectable()
export class AdminMiddleware implements NestMiddleware {
  constructor(private readonly firebaseAdminService: FirebaseAdminService) {}

  async use(req: any, res: any, next: () => void) {
    await this.validateRequest(req);
    next();
  }

  async validateRequest(req: Request): Promise<any> {
    const token = this.getToken(req);
    const payload = await this.verifyToken(token);
    if (payload) {
      return req;
    }
    throw new UnauthorizedException(
      'You are not authorized to perform the operation',
    );
  }

  private async verifyToken(token: string) {
    try {
      return await this.firebaseAdminService
        .getAppFirebase()
        .auth()
        .verifyIdToken(token);
    } catch (error) {
      throw new UnauthorizedException(
        { error },
        'You are not authorized to perform the operation',
      );
    }
  }

  private getToken(req: Request) {
    try {
      return getAuthorizationBearer(req);
    } catch {
      throw new UnauthorizedException(
        {
          error: {
            message: 'No token available',
          },
        },
        'You are not authorized to perform the operation',
      );
    }
  }
}
