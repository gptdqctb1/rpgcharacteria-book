import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { MongooseModule } from '@nestjs/mongoose';
import { CharactersModule } from './characters/characters.module';
import { AdminMiddleware } from './middlewares/admin.middleware';
import { ConfigModule } from '@nestjs/config';
import { FirebaseAdminService } from './service/firebase-admin.service';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MongooseModule.forRoot(
      process.env.MONGO_URI, {
      dbName: 'characters'
    }
    ),
    CharactersModule,
  ],
  controllers: [],
  providers: [FirebaseAdminService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(AdminMiddleware).forRoutes('api/character-db-api/v1/');
  }
}
